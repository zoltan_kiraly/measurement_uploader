import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPConnection {
	private String uploadUrl = "http://localhost:8080/api/uploadMeasurementData";//?signature=59cfe54e9fad3991bc0c6cf45e3c495085c616e4";
	private String loginUrl = "http://localhost:8080/auth/login";
	private String rootUrl = "http://localhost:8080/";
	private String loginParameters = "email=admin%40skawa.hu&password=admin";
	private String cookie = null;
	
	public HTTPConnection() throws Exception{

			HttpURLConnection conn = (HttpURLConnection) new URL(rootUrl).openConnection();
			
			conn.setRequestMethod("GET");
			conn.setInstanceFollowRedirects(false);
			
			cookie = conn.getHeaderFields().get("Set-Cookie").get(0);
		
			conn = (HttpURLConnection) new URL(loginUrl).openConnection();
			conn.setRequestMethod("POST");
			
			if(cookie != null) {
				conn.setRequestProperty("Cookie", cookie);
			}
			
			conn.setInstanceFollowRedirects(false);
			conn.setDoOutput(true);
			DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
			
			
			dos.writeBytes(loginParameters);
			dos.flush(); dos.close();
			
			try(InputStream is = conn.getInputStream();){
				is.close();
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	
	public void sendMeasurement(Measurement m) throws Exception{
		HttpURLConnection conn = (HttpURLConnection) new URL(uploadUrl).openConnection();
		
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Host", "localhost:8080");
		conn.setRequestProperty("Accept", "/");
		conn.setRequestProperty("charsets", "utf-8");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		//if(cookie!=null) conn.setRequestProperty("Cookie", cookie);
		
		conn.setDoOutput(true);
		DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
		
		dos.writeBytes(m.asURLString());
		dos.flush();dos.close();
		
		try(InputStream is = conn.getInputStream();){
			is.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
