import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.UIManager;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.Component;
import java.awt.FlowLayout;
import javax.swing.Box;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {
	
	private JButton uploadBtn;
	private JPanel contentPane;
	private JLabel problemLabel;
	private JLabel successLabel;
	private HTTPConnection connection = null;
	private Measurement  measurement = new Measurement();
	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					try { UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());} 
					catch(Exception e){}
					
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() {
		
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainFrame.class.getResource("/images/icon.png")));
		setTitle("Measurement Uploader");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 292);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel northPanel = new JPanel();
		contentPane.add(northPanel, BorderLayout.NORTH);
		
		JLabel lblEnterTheTime = new JLabel("Enter the GSM info and upload the measurement!");
		lblEnterTheTime.setFont(new Font("Arial", Font.BOLD, 12));
		northPanel.add(lblEnterTheTime);
		
		JPanel southPanel = new JPanel();
		contentPane.add(southPanel, BorderLayout.SOUTH);
		
		problemLabel = new JLabel();
		problemLabel.setForeground(Color.RED);
		southPanel.add(problemLabel);
		
		successLabel = new JLabel();
		successLabel.setForeground(new Color(0, 128, 0));
		southPanel.add(successLabel);
		
		JPanel centerPanel = new JPanel();
		contentPane.add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 20));
		
		JPanel controlPanel = new JPanel();
		centerPanel.add(controlPanel);
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.Y_AXIS));
		
		initConnection();
		
		JPanel cidPanel = new JPanel();
		controlPanel.add(cidPanel);
		
		JLabel lblCid = new JLabel("GsmCellId");
		cidPanel.add(lblCid);
		
		JSpinner spnCid = new JSpinner();
		cidPanel.add(spnCid);
		
		JPanel lacPanel = new JPanel();
		controlPanel.add(lacPanel);
		
		JLabel lblLac = new JLabel("GsmLac   ");
		lacPanel.add(lblLac);
		
		JSpinner spnLac = new JSpinner();
		lacPanel.add(spnLac);
		
		JPanel mccPanel = new JPanel();
		controlPanel.add(mccPanel);
		
		JLabel lblMcc = new JLabel("GsmMcc   ");
		mccPanel.add(lblMcc);
		
		JSpinner spnMcc = new JSpinner();
		mccPanel.add(spnMcc);
		
		JPanel mncPanel = new JPanel();
		controlPanel.add(mncPanel);
		
		JLabel lblMnc = new JLabel("GsmMnc   ");
		mncPanel.add(lblMnc);
		
		JSpinner spnMnc = new JSpinner();
		mncPanel.add(spnMnc);
		
		((JSpinner.DefaultEditor) spnCid.getEditor()).getTextField().setColumns(15);
		((JSpinner.DefaultEditor) spnLac.getEditor()).getTextField().setColumns(15);
		((JSpinner.DefaultEditor) spnMcc.getEditor()).getTextField().setColumns(15);
		((JSpinner.DefaultEditor) spnMnc.getEditor()).getTextField().setColumns(15);
		
		Component verticalStrut_2 = Box.createVerticalStrut(20);
		verticalStrut_2.setPreferredSize(new Dimension(0, 10));
		controlPanel.add(verticalStrut_2);
		
		uploadBtn = new JButton("Send");
		controlPanel.add(uploadBtn);
		uploadBtn.setEnabled(false);
		uploadBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				uploadBtn.setEnabled(false);
				measurement.GsmCellId = (Integer)spnCid.getValue();
				measurement.GsmLac = (Integer)spnLac.getValue();
				measurement.GsmMcc = (Integer)spnMcc.getValue();
				measurement.GsmMnc = (Integer)spnMnc.getValue();
				
				uploadMeasurementData();
				uploadBtn.setEnabled(true);
			}
		});
		uploadBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
		uploadBtn.setEnabled(true);
	}
	
	//Megpróbál csatlakozni a reportinghoz. Az eredményt jelzi szövegesen
	private void initConnection(){
		try{
			connection = new HTTPConnection();
			successLabel.setText("Connection established: ("+sdf.format(new Date(System.currentTimeMillis()))+")");
			problemLabel.setText("");
		}catch(Exception e){
			connection=null;
			problemLabel.setText("Connection failed: ("+sdf.format(new Date(System.currentTimeMillis()))+")");
			successLabel.setText("");
		}
	}
	
	private void uploadMeasurementData(){
	//Ha még nem áll fenn kapcsolat, 5-ször próbálkozik
		uploadBtn.setEnabled(false);
		for(int i = 0; i<5; i++){
			if(connection!=null) break;
			initConnection();
		}
	//Ha még így sem sikerült csatlakozni, visszatérünk.
		if(connection==null){
			uploadBtn.setEnabled(true);
			return;
		}
	//Ha létezett a kapcsolat, vagy most létrehoztuk, megpróbáljuk a küldést
		try {
			connection.sendMeasurement(measurement);
			problemLabel.setText("");
			successLabel.setText("Uploaded successfully: ("+sdf.format(new Date(System.currentTimeMillis()))+")");
		} catch (Exception e) {
			connection=null;
			successLabel.setText("");
			problemLabel.setText("Upload failed: ("+sdf.format(new Date(System.currentTimeMillis()))+")");
		}
		uploadBtn.setEnabled(true);
		return;
	}

}
