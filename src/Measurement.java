import java.net.URLEncoder;

public class Measurement {

	public String scanningTime = "2016-04-10T13:00:00Z";
	public long GsmCellId = 0;
	public int GsmLac = 0;
	public int GsmMcc = 0;
	public int GsmMnc = 0;
	
	public String scannerControl = "";
	public boolean sendScannerControl = true;
	
	public String asJSONString(){
		return "{"
				+ "\"Comment\":\"TEST MEASUREMENT\","
				+ "\"DepartmentID\":5,"
				+ "\"ErrorCode\":\"\","
				+ "\"ErrorLog\":0,"
				+ "\"GsmCellId\":"+GsmCellId+","
				+ "\"GsmLac\":"+GsmLac+","
				+ "\"GsmMcc\":"+GsmMcc+","
				+ "\"GsmMnc\":"+GsmMnc+","
				+ "\"HWVersion\":\"\","
				+ "\"HandRubCount\":0,"
				+ "\"LeftDorsumCoverage\":1.0,"
				+ "\"LeftDorsumRegions\":[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],"
				+ "\"LeftDorsumResult\":true,"
				+ "\"LeftHandRing\":false,"
				+ "\"LeftPalmCoverage\":1.0,"
				+ "\"LeftPalmRegions\":[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],"
				+ "\"LeftPalmResult\":true,"
				+ "\"LoginTime\":\"2017-03-20T08:48:33Z\","
				+ "\"MeasurementStartTime\":\"2017-04-10T08:49:40Z\","
				+ "\"RFID\":\"HinS-R-abcd0\","
				+ "\"LeftPalmEvaluation\":\"0\","
				+ "\"LeftDorsumEvaluation\":\"0\","
				+ "\"RightPalmEvaluation\":\"0\","
				+ "\"RightDorsumEvaluation\":\"0\","
				+ "\"ReferenceImage\":\"\","
				+ "\"RightDorsumCoverage\":1.0,"
				+ "\"RightDorsumRegions\":[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],"
				+ "\"RightDorsumResult\":true,"
				+ "\"RightHandRing\":false,"
				+ "\"RightPalmCoverage\":1.0,"
				+ "\"RightPalmRegions\":[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],"
				+ "\"RightPalmResult\":true,"
				+ "\"SWVersion\":\"\","
				+ "\"ScannerID\":\"HP_TEST1\","
			    //uuid
				+ ((sendScannerControl)?("\"scannerControlVersion\":\""+scannerControl+"\","):"")
				+ "\"ScanningTime\":\""+scanningTime+"\""
				+ "}";
	}
	
	public String asURLString(){
		try { return "data="+URLEncoder.encode(this.asJSONString(), "UTF-8");
		} catch (Exception e) {return null;}
		
	}
}
